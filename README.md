# OS internals scraping toolkit

This project collects information about data structures and system calls
from several sources and combines them into a single, script-consumable
data set.

Known inaccuracies in the collected data are automatically corrected.

## Dependencies

- make
- Python 2.7

The toolkit uses the following Python libraries:

- pdbparse
- construct==2.5.2 (required by pdbparse)
- bs4
- jinja2
- pytest
- mock
- requests
