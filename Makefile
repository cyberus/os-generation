ARTIFACTS_DIR = artifacts
WINDOWS_DIR = windows

all: syscall_signatures windows_types

syscall_signatures:
	python $(WINDOWS_DIR)/syscall_signature_parser/syscall_signature_parser.py $(ARTIFACTS_DIR)/system_calls_nt_64.csv $(ARTIFACTS_DIR)/system_call_signatures.json

windows_types:
	python $(WINDOWS_DIR)/download_ntoskrnl_pdb.py --csv_filepath $(ARTIFACTS_DIR)/versions_ntoskrnl.csv --out_file $(ARTIFACTS_DIR)/win_types_ntoskrnl.json

clean:
	rm $(ARTIFACTS_DIR)/win_types_ntoskrnl.json
	rm $(ARTIFACTS_DIR)/system_call_signatures.json
