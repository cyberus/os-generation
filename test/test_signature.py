#!/usr/bin/env python3

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" Sanity check generated artifact files:
    system_call_signatures.json
    win_types_ntoskrnl.json
"""

import unittest
import json
import re
import pytest
import csv

reghex = r"^0x[\da-f]+$"
word = r"^\w+$"

def read_syscall_names(csv_filepath):
    """ Reads syscall names out of a csv file. """
    with open(csv_filepath, "r") as csvfile:
        content = list(csv.reader(csvfile, delimiter=';'))
        return [name[0] for name in content[1:]]

def get_signature_file(json_file):
    """ This function opens and loads system call signatures from a json file. """
    with open(json_file, "r") as j_file:
        return json.load(j_file)

class TestSyscallSignature(unittest.TestCase):
    """ These tests sanity check the system call signatures. """
    def setUp(self):
        signature_file = "artifacts/system_call_signatures.json"
        self.json_file = get_signature_file(signature_file)

    def test_syscall_signature_general_structure(self):
        assert isinstance(self.json_file, dict)

    def test_syscall_signature_names_valid(self):
        for name in self.json_file.keys():
            assert re.match("^Nt\w+$", name)

    def test_syscall_signature_list_is_complete(self):
        syscall_names = read_syscall_names("artifacts/system_calls_nt_64.csv")
        for syscall_name in syscall_names:
            assert syscall_name in self.json_file.keys(), "Syscall {} not found in signatures.".format(syscall_name)

    def test_syscall_signature_value_structure(self):
        for values in self.json_file.values():
            if values:
                assert isinstance(values, list)
                for typ, name in values:
                    assert re.match(word, name)
                    ptyp, tname = typ
                    assert re.match("^(IN|OUT|INOUT)$", ptyp)
                    assert re.match(word, tname)

class TestWindowsTypes(unittest.TestCase):
    """ These tests sanity check the windows type signatures (structs, enums). """
    def setUp(self):
        type_file = "artifacts/win_types_ntoskrnl.json"
        self.typ_file = get_signature_file(type_file)

    def test_general_signature(self):
        assert isinstance(self.typ_file, dict)

    def test_windows_version_names_valid(self):
        for name in self.typ_file.keys():
            assert re.match("^win7_x[\d_]+$", name)

    def test_enum_type_structure(self):
        for _, enums in self.typ_file.values():
            assert isinstance(enums, dict)
            for name in enums.keys():
                assert re.match(word, name), "Undefined character in {}".format(name)

                for member_name, member_val in enums[name]:
                    assert re.match(word, member_name), "Undefined character in {}".format(member_name)
                    # some enums have -0x1, probably to indicate max unsigned value
                    assert re.match("^-?0x[\da-f]+$", member_val), "Undefined character in {}".format(member_val)

    def test_windows_type_value_structure(self):
        for structs, _  in self.typ_file.values():
            assert isinstance(structs, dict)
            for name in structs.keys():
                assert re.match(word, name)
            for size, members in structs.values():
                assert re.match(reghex, size)
                assert isinstance(members, list)
                for name, offset, typ in members:
                    assert not typ.startswith("PT_"), \
                        "Invalid pointer type. Is there a missing type in type_generator.py:type_to_pointer()?"
                    assert re.match(word, name)
                    assert re.match(reghex, offset)
                    assert re.match("^[\w\*]+$", typ)
