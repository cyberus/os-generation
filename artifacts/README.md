This file describes the layout of the json files in this artifacts folder.

# system_call_signatures.json

The toplevel structure of this json file is a dictionary.
The system call names are used as keys.
The values contain a list of system call parameters, that are represented as follows:
The first part of the tuple contains type information, which is also a tuple.
The type information tuple contains the parameter type (IN,OUT,INOUT) and the type
The second part is the parameter name.

```json
{ syscall_name : [ [[IN, type1], parameter1], [[OUT, type2], parameter2] ] }
```


# win_types_ntoskrnl.json

The toplevel structure of this json file is a dictionary.
The key is the windows version and the value is a list of currently two items.
The first item is a dict of structures, that are represented as follows:
The key is the structure name and the value a tuple of structure size and list of members.
The list elements are tuples. The first tuple element is the member name, the seconde the offset within the structure, and the third the data type.

The second list item is a dictionary of enum types.
The key is the enumeration name, and the value a list of tuples.
These tuples contains the name of the enum value as first part, the second contains the enumerator.

```python
{ windows_version :
    [
        {
            structure_name : [ size, [ [membername1, offset1, type1], [membername2, offset2, type2]] ]
        },
        {
            enum_name : [ [enumerator_name, enumerator], [enumerator_name2, enumerator] ]
        }
    ]
}
```
