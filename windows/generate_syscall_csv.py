#! /usr/bin/env python3

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
 This module generates csv files containing the windows system call
 names and the corresponding system call number for every windows
 version and service pack.

"""

import urllib.request as urllib
import os
import pathlib
import csv
from bs4 import BeautifulSoup

JOORU_URL_NT_32     = "http://j00ru.vexillium.org/syscalls/nt/32/"
JOORU_URL_NT_64     = "http://j00ru.vexillium.org/syscalls/nt/64/"
JOORU_URL_WIN32k_32 = "http://j00ru.vexillium.org/syscalls/win32k/32/"
JOORU_URL_WIN32k_64 = "http://j00ru.vexillium.org/syscalls/win32k/64/"
CSV_DIRECTORY       = "../artifacts"
OUT_FILENAME        = "system_calls"
SYSCALL_TABLE       = 1
OS_ROW              = 0
OS_VERSION_ROW      = 1
SYSCALL_ROW_START   = 2

def generate_outfile_suffix(url):
    """ autogenerate filename suffix from jooru url. Expect url ending like "/nt/32/". """
    return "_".join(url.split("/")[-3:-1])

def mk_int(s, base = 16):
    """ Convert string to integer and return the integer, or -1 if string contains no numeric value. """
    s = s.strip()
    return int(s, base) if s else -1

def os_id_to_number(os_id):
    """ Convert os ID format used in the html table to a number. """
    tmp = os_id.replace("os_", "")
    return int(tmp)

def get_os_list(syscall_table_rows):
    """ Parses the html table row of operating systems + service packs and return list containing the os names.

    Keyword arguments:
    syscall_table_rows -- the html table rows containing the system call table
    """
    assert (syscall_table_rows[OS_ROW].attrs["class"][0]         == "header"), "Table row has wrong class ID"
    assert (syscall_table_rows[OS_VERSION_ROW].attrs["class"][0] == "header"), "Table row has wrong class ID"
    os_col        = syscall_table_rows[OS_ROW].find_all("td")
    os_tuples     = [(int(td.attrs["colspan"]), td.text.replace("(show)","").strip()) for td in os_col[1:]]
    ver_list      = [td.attrs["text"] for td in syscall_table_rows[OS_VERSION_ROW].find_all("td")]
    expanded_oses = sum([x * [name] for x, name in os_tuples], [])
    return [os + " " + sp for os, sp in zip(expanded_oses, ver_list)]

def write_csv(outfile, syscall_dict):
    """Write the system call information into a csv table.
    Column represents operating systems.
    Row represents system calls.

    Keyword arguments:
    outfile      -- the filename to write the csv data
    syscall_dict -- System call dictionary with structure: {"os_name" : {"syscall_name" : syscall_number}}
    """
    with open(outfile, "w") as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=";", lineterminator='\n')
        os_list = list(syscall_dict)
        csvwriter.writerow(["System Call Name"] + os_list)

        # every os contains every syscall, thus take first to get list of syscall names
        for syscall in sorted(syscall_dict[list(syscall_dict)[0]]):
            csvwriter.writerow([syscall] + [syscall_dict[os][syscall] for os in os_list])

def parse_html(jooru_url):
    """Retrieve jooru website, parse content and return as dictionary.

    Keyword arguments:
    jooru_url -- the jooru url to the system call information
    """
    page = urllib.urlopen(jooru_url)
    soup = BeautifulSoup(page, "html.parser")
    tables    = soup.find_all("table")
    sys_table = tables[SYSCALL_TABLE]
    rows      = sys_table.find_all("tr")
    syscall_rows = [n.find_all("td") for n in rows[SYSCALL_ROW_START:]]
    os_list   = get_os_list(rows)
    syscall_dict = dict()
    for row in syscall_rows:
        syscall_name = row[0].contents[0]
        syscall_list = [mk_int(sys_num.attrs["text"], 16) for sys_num in row[1:]]
        for idx, os in enumerate(os_list):
            syscall_dict.setdefault(os, dict())[syscall_name] = syscall_list[idx]
    return syscall_dict

def generate_syscall_csv(jooru_url):
    """Parse a given jooru url and write content into csv file mapping an operating system and
    a system call name to a system call number used by Windows internally.

    Keyword arguments:
    jooru_url -- the jooru url to the system call information
    """
    pathlib.Path(CSV_DIRECTORY).mkdir(parents=True, exist_ok=True)
    outname = OUT_FILENAME + "_" + generate_outfile_suffix(jooru_url) + ".csv"
    outfile = os.path.join(CSV_DIRECTORY, outname)
    write_csv(outfile, parse_html(jooru_url))


if __name__ == "__main__":
    generate_syscall_csv(JOORU_URL_NT_32)
    generate_syscall_csv(JOORU_URL_NT_64)
    generate_syscall_csv(JOORU_URL_WIN32k_32)
    generate_syscall_csv(JOORU_URL_WIN32k_64)
