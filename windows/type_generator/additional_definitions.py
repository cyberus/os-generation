#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" This module contains missing type information as customly defined dicts. Those are assumed
to be the same for all windows versions. The definitions are retrieved manually as the information
is spreaded across multiple sources.
"""

additional_structs= {
    # https://msdn.microsoft.com/de-de/library/windows/desktop/aa379595(v=vs.85).aspx
    "SID_AND_ATTRIBUTES" : (
        0xC, [("Sid", 0x0, "VOID*"), ("Attributes", 0x8, "ULONG")]
    ),

    # https://msdn.microsoft.com/de-de/library/windows/desktop/aa379634(v=vs.85).aspx
    "TOKEN_USER" : (
        0xC, [("User", 0x0, "SID_AND_ATTRIBUTES")]
    ),

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FThread%2FNtCreateThread.html
    "INITIAL_TEB" : (
        0x28, [("StackBase", 0x0, "VOID*"),
               ("StackLimit", 0x8, "VOID*"),
               ("StackCommit", 0x8, "VOID*"),
               ("StackCommitMax", 0x8, "VOID*"),
               ("StackReserved", 0x8, "VOID*")]
    ),

    # https://msdn.microsoft.com/en-us/library/windows/desktop/aa379629(v=vs.85).aspx
    "TOKEN_PRIMARY_GROUP" : (
        0x8, [("PrimaryGroup", 0x0, "SID*")]
    ),

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FPort%2FLPC_SECTION_MEMORY.html
    "LPC_SECTION_MEMORY" : (
        0x10, [("Length", 0x0, "ULONG"), ("ViewSize", 0x4, "ULONG"), ("ViewBase", 0x8, "VOID*")]
    ),

    # https://msdn.microsoft.com/de-de/library/windows/desktop/aa379623(v=vs.85).aspx
    "TOKEN_DEFAULT_DACL" : (
        0x8, [("DefaultDacl", 0x0, "ACL*")]
    ),

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FPort%2FLPC_SECTION_OWNER_MEMORY.html
    "LPC_SECTION_OWNER_MEMORY" : (
        0x24, [("Length", 0x0, "ULONG"),
               ("SectionHandle", 0x4, "HANDLE"),
               ("OffsetInSection", 0xC, "ULONG"),
               ("ViewSize", 0x10, "ULONG"),
               ("ViewBase", 0x14, "VOID*"),
               ("OtherSideViewBase", 0x1c, "VOID*")]
    ),

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FKey%2FKEY_MULTIPLE_VALUE_INFORMATION.html
    "KEY_MULTIPLE_VALUE_INFORMATION" : (
        0x14, [("ValueName", 0x0, "PUNICODE_STRING"),
               ("DataLength", 0x8, "ULONG"),
               ("DataOffset", 0xC, "ULONG"),
               ("Type", 0x10, "ULONG")]
    ),

    # https://msdn.microsoft.com/de-de/library/bb470238(v=vs.85).aspx
    "OBJDIR_INFORMATION" : (
        0x20, [("ObjectName", 0x0, "UNICODE_STRING"),
               ("ObjectTypeName", 0x10, "UNICODE_STRING")]
    ),

    # https://github.com/gratianlup/SecureDelete/blob/master/SecureDeleteNative/ntfsContext.h
    "IO_APC_ROUTINE" : (
        0x1C, [("ApcContext", 0x0, "VOID*"),
               ("IoStatusBlock", 0x8, "IO_STATUS_BLOCK*"),
               ("reserved", 0x18, "ULONG")]
    ),

    # https://msdn.microsoft.com/de-de/library/windows/desktop/aa379628(v=vs.85).aspx
    "TOKEN_OWNER" : (
        0x8, [("Owner", 0x0, "SID*")]
    ),

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FPort%2FLPC_MESSAGE.html
    "LPC_MESSAGE" : (
        0x20, [
               ("DataLength", 0x0, "USHORT"),
               ("Length", 0x2, "USHORT"),
               ("MessageType", 0x4, "USHORT"),
               ("DataInfoOffset", 0x6, "USHORT"),
               ("ClientId", 0x8, "CLIENT_ID"),
               ("MessageId", 0x18, "ULONG"),
               ("CallbackId", 0x1C, "ULONG")]
    ),

    # https://msdn.microsoft.com/de-de/library/windows/desktop/aa379624(v=vs.85).aspx
    "TOKEN_GROUPS" : (
        0x10, [("GroupCount", 0x0, "ULONG"), ("Groups", 0x4, "SID_AND_ATTRIBUTES*")]
    ),
}

additional_enums= {
    # https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/ntifs/ne-ntifs-_memory_information_class
    "MEMORY_INFORMATION_CLASS" : [("MemoryBasicInformation", 0x0)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FSECTION_INHERIT.html
    "SECTION_INHERIT" : [("ViewShare", 0x0), ("ViewUnmap", 0x1)],

    # http://www.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FEvent%2FEVENT_INFORMATION_CLASS.html
    "EVENT_INFORMATION_CLASS" : [("EventBasicInformation", 0x0)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FAtoms%2FNtQueryInformationAtom.html
    "ATOM_INFORMATION_CLASS" : [("AtomBasicInformation", 0x0), ("AtomTableInformation", 0x1)],

    # https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/wdm/ne-wdm-_fsinfoclass
    "FS_INFORMATION_CLASS" : [
        ("FileFsVolumeInformation", 0x0),
        ("FileFsLabelInformation", 0x1),
        ("FileFsSizeInformation", 0x2),
        ("FileFsDeviceInformation", 0x3),
        ("FileFsAttributeInformation", 0x4),
        ("FileFsControlInformation", 0x5),
        ("FileFsFullSizeInformation", 0x6),
        ("FileFsObjectIdInformation", 0x7),
        ("FileFsDriverPathInformation", 0x8),
        ("FileFsVolumeFlagsInformation", 0x9),
        ("FileFsSectorSizeInformation", 0xA),
        ("FileFsDataCopyInformation", 0xB),
        ("FileFsMetadataSizeInformation", 0XC),
        ("FileFsMaximumInformation", 0xD)
    ],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FMutant%2FNtQueryMutant.html
    "MUTANT_INFORMATION_CLASS" : [("MutantBasicInformation", 0x0)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FIoCompletion%2FIO_COMPLETION_INFORMATION_CLASS.html
    "IO_COMPLETION_INFORMATION_CLASS" : [("IoCompletionBasicInformation", 0x0)],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FTimer%2FNtQueryTimer.html
    "TIMER_INFORMATION_CLASS" : [("TimerBasicInformation", 0x0)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FProcess%2FPROCESS_INFORMATION_CLASS.html
    "PROCESS_INFORMATION_CLASS" : [
        ("ProcessBasicInformation", 0x0),
        ("ProcessQuotaLimits", 0x1),
        ("ProcessIoCounters", 0x2),
        ("ProcessVmCounters", 0x3),
        ("ProcessTimes", 0x4),
        ("ProcessBasePriority", 0x5),
        ("ProcessRaisePriority", 0x6),
        ("ProcessDebugPort", 0x7),
        ("ProcessExceptionPort", 0x8),
        ("ProcessAccessToken", 0x9),
        ("ProcessLdtInformation", 0xA),
        ("ProcessLdtSize", 0xB),
        ("ProcessDefaultHardErrorMode", 0xC),
        ("ProcessIoPortHandlers", 0xD),
        ("ProcessPooledUsageAndLimits", 0xE),
        ("ProcessWorkingSetWatch", 0xF),
        ("ProcessUserModeIOPL", 0x10),
        ("ProcessEnableAlignmentFaultFixup", 0x11),
        ("ProcessPriorityClass", 0x12),
        ("ProcessWx86Information", 0x13),
        ("ProcessHandleCount", 0x14),
        ("ProcessAffinityMask", 0x15),
        ("ProcessPriorityBoost", 0x16),
        ("MaxProcessInfoClass", 0x17)
        ],

    # http://www.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FType%20independed%2FOBJECT_WAIT_TYPE.html
    "OBJECT_WAIT_TYPE" : [(" WaitAllObject", 0x0), ("WaitAnyObject", 0x1)],

    # https://www.geoffchappell.com/studies/windows/km/ntoskrnl/api/ex/sysinfo/class.htm
    "SYSTEM_INFORMATION_CLASS" : [
        ("SystemBasicInformation", 0x00),
        ("SystemProcessorInformation", 0x01),
        ("SystemPerformanceInformation", 0x02),
        ("SystemTimeOfDayInformation", 0x03),
        ("SystemPathInformation", 0x04),
        ("SystemProcessInformation", 0x05),
        ("SystemCallCountInformation", 0x06),
        ("SystemDeviceInformation", 0x07),
        ("SystemProcessorPerformanceInformation", 0x08),
        ("SystemFlagsInformation", 0x09),
        ("SystemCallTimeInformation", 0x0A),
        ("SystemModuleInformation", 0x0B),
        ("SystemLocksInformation", 0x0C),
        ("SystemStackTraceInformation", 0x0D),
        ("SystemPagedPoolInformation", 0x0E),
        ("SystemNonPagedPoolInformation", 0x0F),
        ("SystemHandleInformation", 0x10),
        ("SystemObjectInformation", 0x11),
        ("SystemPageFileInformation", 0x12),
        ("SystemVdmInstemulInformation", 0x13),
        ("SystemVdmBopInformation", 0x14),
        ("SystemFileCacheInformation", 0x15),
        ("SystemPoolTagInformation", 0x16),
        ("SystemInterruptInformation", 0x17),
        ("SystemDpcBehaviorInformation", 0x18),
        ("SystemFullMemoryInformation", 0x19),
        ("SystemLoadGdiDriverInformation", 0x1A),
        ("SystemUnloadGdiDriverInformation", 0x1B),
        ("SystemTimeAdjustmentInformation", 0x1C),
        ("SystemSummaryMemoryInformation", 0x1D),
        ("SystemMirrorMemoryInformation", 0x1E),
        ("SystemPerformanceTraceInformation", 0x1F),
        ("SystemObsolete0", 0x20),
        ("SystemExceptionInformation", 0x21),
        ("SystemCrashDumpStateInformation", 0x22),
        ("SystemKernelDebuggerInformation", 0x23),
        ("SystemContextSwitchInformation", 0x24),
        ("SystemRegistryQuotaInformation", 0x25),
        ("SystemExtendedServiceTableInformation", 0x26),
        ("SystemPrioritySeparation", 0x27),
        ("SystemVerifierAddDriverInformation", 0x28),
        ("SystemVerifierRemoveDriverInformation", 0x29),
        ("SystemProcessorIdleInformation", 0x2A),
        ("SystemLegacyDriverInformation", 0x2B),
        ("SystemCurrentTimeZoneInformation", 0x2C),
        ("SystemLookasideInformation", 0x2D),
        ("SystemTimeSlipNotification", 0x2E),
        ("SystemSessionCreate", 0x2F),
        ("SystemSessionDetach", 0x30),
        ("SystemSessionInformation", 0x31),
        ("SystemRangeStartInformation", 0x32),
        ("SystemVerifierInformation", 0x33),
        ("SystemVerifierThunkExtend", 0x34),
        ("SystemSessionProcessInformation", 0x35),
        ("SystemLoadGdiDriverInSystemSpace", 0x36),
        ("SystemNumaProcessorMap", 0x37),
        ("SystemPrefetcherInformation", 0x38),
        ("SystemExtendedProcessInformation", 0x39),
        ("SystemRecommendedSharedDataAlignment", 0x3A),
        ("SystemComPlusPackage", 0x3B),
        ("SystemNumaAvailableMemory", 0x3C),
        ("SystemProcessorPowerInformation", 0x3D),
        ("SystemEmulationBasicInformation", 0x3E),
        ("SystemEmulationProcessorInformation", 0x3F),
        ("SystemExtendedHandleInformation", 0x40),
        ("SystemLostDelayedWriteInformation", 0x41),
        ("SystemBigPoolInformation", 0x42),
        ("SystemSessionPoolTagInformation", 0x43),
        ("SystemSessionMappedViewInformation", 0x44),
        ("SystemHotpatchInformation", 0x45),
        ("SystemObjectSecurityMode", 0x46),
        ("SystemWatchdogTimerHandler", 0x47),
        ("SystemWatchdogTimerInformation", 0x48),
        ("SystemLogicalProcessorInformation", 0x49),
        ("SystemWow64SharedInformationObsolete", 0x4A),
        ("SystemRegisterFirmwareTableInformationHandler", 0x4B),
        ("SystemFirmwareTableInformation", 0x4C),
        ("SystemModuleInformationEx", 0x4D),
        ("SystemVerifierTriageInformation", 0x4E),
        ("SystemSuperfetchInformation", 0x4F),
        ("SystemMemoryListInformation", 0x50),
        ("SystemFileCacheInformationEx", 0x51),
        ("SystemThreadPriorityClientIdInformation", 0x52),
        ("SystemProcessorIdleCycleTimeInformation", 0x53),
        ("SystemVerifierCancellationInformation", 0x54),
        ("SystemProcessorPowerInformationEx", 0x55),
        ("SystemRefTraceInformation", 0x56),
        ("SystemSpecialPoolInformation", 0x57),
        ("SystemProcessIdInformation", 0x58),
        ("SystemErrorPortInformation", 0x59),
        ("SystemBootEnvironmentInformation", 0x5A),
        ("SystemHypervisorInformation", 0x5B),
        ("SystemVerifierInformationEx", 0x5C),
        ("SystemTimeZoneInformation", 0x5D),
        ("SystemImageFileExecutionOptionsInformation", 0x5E),
        ("SystemCoverageInformation", 0x5F),
        ("SystemPrefetchPatchInformation", 0x60),
        ("SystemVerifierFaultsInformation", 0x61),
        ("SystemSystemPartitionInformation", 0x62),
        ("SystemSystemDiskInformation", 0x63),
        ("SystemProcessorPerformanceDistribution", 0x64),
        ("SystemNumaProximityNodeInformation", 0x65),
        ("SystemDynamicTimeZoneInformation", 0x66),
        ("SystemCodeIntegrityInformation", 0x67),
        ("SystemProcessorMicrocodeUpdateInformation", 0x68),
        ("SystemProcessorBrandString", 0x69),
        ("SystemVirtualAddressInformation", 0x6A),
        ("SystemLogicalProcessorAndGroupInformation", 0x6B),
        ("SystemProcessorCycleTimeInformation", 0x6C),
        ("SystemStoreInformation", 0x6D),
        ("SystemRegistryAppendString", 0x6E),
        ("SystemAitSamplingValue", 0x6F),
        ("SystemVhdBootInformation", 0x70),
        ("SystemCpuQuotaInformation", 0x71),
        ("SystemNativeBasicInformation", 0x72),
        ("SystemErrorPortTimeouts", 0x73),
        ("SystemLowPriorityIoInformation", 0x74),
        ("SystemBootEntropyInformation", 0x75),
        ("SystemVerifierCountersInformation", 0x76),
        ("SystemPagedPoolInformationEx", 0x77),
        ("SystemSystemPtesInformationEx", 0x78),
        ("SystemNodeDistanceInformation", 0x79),
        ("SystemAcpiAuditInformation", 0x7A),
        ("SystemBasicPerformanceInformation", 0x7B),
        ("SystemQueryPerformanceCounterInformation", 0x7C),
        ("SystemSessionBigPoolInformation", 0x7D),
        ("SystemBootGraphicsInformation", 0x7E),
        ("SystemScrubPhysicalMemoryInformation", 0x7F),
        ("SystemBadPageInformation", 0x80),
        ("SystemProcessorProfileControlArea", 0x81),
        ("SystemCombinePhysicalMemoryInformation", 0x82),
        ("SystemEntropyInterruptTimingInformation", 0x83),
        ("SystemConsoleInformation", 0x84),
        ("SystemPlatformBinaryInformation", 0x85),
        ("SystemThrottleNotificationInformation", 0x86),
        ("SystemHypervisorProcessorCountInformation", 0x87),
        ("SystemDeviceDataInformation", 0x88),
        ("SystemDeviceDataEnumerationInformation", 0x89),
        ("SystemMemoryTopologyInformation", 0x8A),
        ("SystemMemoryChannelInformation", 0x8B),
        ("SystemBootLogoInformation", 0x8C),
        ("SystemProcessorPerformanceInformationEx", 0x8D),
        ("SystemSpare0", 0x8E),
        ("SystemSecureBootPolicyInformation", 0x8F),
        ("SystemPageFileInformationEx", 0x90),
        ("SystemSecureBootInformation", 0x91),
        ("SystemEntropyInterruptTimingRawInformation", 0x92),
        ("SystemPortableWorkspaceEfiLauncherInformation", 0x93),
        ("SystemFullProcessInformation", 0x94),
        ("SystemKernelDebuggerInformationEx", 0x95),
        ("SystemBootMetadataInformation", 0x96),
        ("SystemSoftRebootInformation", 0x97),
        ("SystemElamCertificateInformation", 0x98),
        ("SystemOfflineDumpConfigInformation", 0x99),
        ("SystemProcessorFeaturesInformation", 0x9A),
        ("SystemRegistryReconciliationInformation", 0x9B),
        ("SystemEdidInformation", 0x9C),
    ],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FError%2FHARDERROR_RESPONSE_OPTION.html
    "HARDERROR_RESPONSE_OPTION" : [
        ("OptionAbortRetryIgnore", 0x0),
        ("OptionOk", 0x1),
        ("OptionOkCancel", 0x2),
        ("OptionRetryCancel", 0x3),
        ("OptionYesNo", 0x4),
        ("OptionYesNoCancel", 0x5),
        ("OptionShutdownSystem", 0x6)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FTimer%2FNtCreateTimer.html
    "TIMER_TYPE" : [("NotificationTimer", 0x0), ("SynchronizationTimer", 0x1)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSemaphore%2FSEMAPHORE_BASIC_INFORMATION.html
    "SEMAPHORE_INFORMATION_CLASS" : [("SemaphoreBasicInformation", 0x0)],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FProfile%2FKPROFILE_SOURCE.html
    "KPROFILE_SOURCE" : [
        ("ProfileTime", 0x0),
        ("ProfileAlignmentFixup", 0x1),
        ("ProfileTotalIssues", 0x2),
        ("ProfilePipelineDry", 0x3),
        ("ProfileLoadInstructions", 0x4),
        ("ProfilePipelineFrozen", 0x5),
        ("ProfileBranchInstructions", 0x6),
        ("ProfileTotalNonissues", 0x7),
        ("ProfileDcacheMisses", 0x8),
        ("ProfileIcacheMisses", 0x9),
        ("ProfileCacheMisses", 0xA),
        ("ProfileBranchMispredictions", 0xB),
        ("ProfileStoreInstructions", 0xC),
        ("ProfileFpInstructions", 0xD),
        ("ProfileIntegerInstructions", 0xE),
        ("Profile2Issue", 0xF),
        ("Profile3Issue", 0x10),
        ("Profile4Issue", 0x11),
        ("ProfileSpecialInstructions", 0x12),
        ("ProfileTotalCycles", 0x13),
        ("ProfileIcacheIssues", 0x14),
        ("ProfileDcacheAccesses", 0x15),
        ("ProfileMemoryBarrierCycles", 0x16),
        ("ProfileLoadLinkedIssues", 0x17),
        ("ProfileMaximum", 0x18)
    ],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FHardware%2FNtShutdownSystem.html
    "SHUTDOWN_ACTION" : [("ShutdownNoReboot", 0x0), ("ShutdownReboot", 0x1), ("ShutdownPowerOff", 0x2)],

    # http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FPort%2FPORT_INFORMATION_CLASS.html
    "PORT_INFORMATION_CLASS" : [("PortNoInformation", 0x0)],

    # https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/wdm/ne-wdm-_key_set_information_class
    "KEY_SET_INFORMATION_CLASS" : [
        ("KeyWriteTimeInformation", 0x0),
        ("KeyWow64FlagsInformation", 0x1),
        ("KeyControlFlagsInformation", 0x2),
        ("KeySetVirtualizationInformation", 0x3),
        ("KeySetDebugInformation", 0x4),
        ("KeySetHandleTagsInformation", 0x5),
        ("KeySetLayerInformation", 0x6),
        ("MaxKeySetInfoClass", 0x7)
    ],

    # http://www.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FSECTION_INFORMATION_CLASS.html
    "SECTION_INFORMATION_CLASS" : [("SectionBasicInformation", 0x0), ("SectionImageInformation", 0x1)],

    # https://msdn.microsoft.com/en-us/library/windows/desktop/aa379626(v=vs.85).aspx
    "TOKEN_INFORMATION_CLASS" : [
        ("TokenUser", 0x1),
        ("TokenGroups", 0x2),
        ("TokenPrivileges", 0x3),
        ("TokenOwner", 0x4),
        ("TokenPrimaryGroup", 0x5),
        ("TokenDefaultDacl", 0x6),
        ("TokenSource", 0x7),
        ("TokenType", 0x8),
        ("TokenImpersonationLevel", 0x9),
        ("TokenStatistics", 0xA),
        ("TokenRestrictedSids", 0xB),
        ("TokenSessionId", 0xC),
        ("TokenGroupsAndPrivileges", 0xD),
        ("TokenSessionReference", 0xE),
        ("TokenSandBoxInert", 0xF),
        ("TokenAuditPolicy", 0x10),
        ("TokenOrigin", 0x11),
        ("TokenElevationType", 0x12),
        ("TokenLinkedToken", 0x13),
        ("TokenElevation", 0x14),
        ("TokenHasRestrictions", 0x15),
        ("TokenAccessInformation", 0x16),
        ("TokenVirtualizationAllowed", 0x17),
        ("TokenVirtualizationEnabled", 0x18),
        ("TokenIntegrityLevel", 0x19),
        ("TokenUIAccess", 0x1A),
        ("TokenMandatoryPolicy", 0x1B),
        ("TokenLogonSid", 0x1C),
        ("TokenIsAppContainer", 0x1D),
        ("TokenCapabilities", 0x1E),
        ("TokenAppContainerSid", 0x1F),
        ("TokenAppContainerNumber", 0x20),
        ("TokenUserClaimAttributes", 0x21),
        ("TokenDeviceClaimAttributes", 0x22),
        ("TokenRestrictedUserClaimAttributes", 0x23),
        ("TokenRestrictedDeviceClaimAttributes", 0x24),
        ("TokenDeviceGroups", 0x25),
        ("TokenRestrictedDeviceGroups", 0x26),
        ("TokenSecurityAttributes", 0x27),
        ("TokenIsRestricted", 0x28),
        ("MaxTokenInfoClass", 0x29)
    ],

    # http://ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FDebug%2FSYSDBG_COMMAND.html
    "SYSDBG_COMMAND" : [
        ("SysDbgQueryModuleInformation", 0x1),
        ("SysDbgQueryTraceInformation", 0x2),
        ("SysDbgSetTracepoint", 0x3),
        ("SysDbgSetSpecialCall", 0x4),
        ("SysDbgClearSpecialCalls", 0x5),
        ("SysDbgQuerySpecialCalls", 0x6)
    ],

    # https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/wdm/ne-wdm-_key_information_class
    "KEY_INFORMATION_CLASS" : [
        ("KeyBasicInformation", 0x0),
        ("KeyNodeInformation", 0x1),
        ("KeyFullInformation", 0x2),
        ("KeyNameInformation", 0x3),
        ("KeyCachedInformation", 0x4),
        ("KeyFlagsInformation", 0x5),
        ("KeyVirtualizationInformation", 0x6),
        ("KeyHandleTagsInformation", 0x7),
        ("KeyTrustInformation", 0x8),
        ("KeyLayerInformation", 0x9),
        ("MaxKeyInfoClass", 0xA)
    ],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FError%2FHARDERROR_RESPONSE.html
    "HARDERROR_RESPONSE" : [
        ("ResponseReturnToCaller", 0x0),
        ("ResponseNotHandled", 0x1),
        ("ResponseAbort", 0x2),
        ("ResponseCancel", 0x3),
        ("ResponseIgnore", 0x4),
        ("ResponseNo", 0x5),
        ("ResponseOk", 0x6),
        ("ResponseRetry", 0x7),
        ("ResponseYes", 0x8)
    ],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FThread%2FTHREAD_INFORMATION_CLASS.html
    "THREAD_INFORMATION_CLASS" : [
        ("ThreadBasicInformation", 0x0),
        ("ThreadTimes", 0x1),
        ("ThreadPriority", 0x2),
        ("ThreadBasePriority", 0x3),
        ("ThreadAffinityMask", 0x4),
        ("ThreadImpersonationToken", 0x5),
        ("ThreadDescriptorTableEntry", 0x6),
        ("ThreadEnableAlignmentFaultFixup", 0x7),
        ("ThreadEventPair", 0x8),
        ("ThreadQuerySetWin32StartAddress", 0x9),
        ("ThreadZeroTlsCell", 0xA),
        ("ThreadPerformanceCount", 0xB),
        ("ThreadAmILastThread", 0xC),
        ("ThreadIdealProcessor", 0xD),
        ("ThreadPriorityBoost", 0xE),
        ("ThreadSetTlsArrayAddress", 0xF),
        ("ThreadIsIoPending", 0x10),
        ("ThreadHideFromDebugger", 0x11)
    ],

    # https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/wdm/ne-wdm-_key_value_information_class
    "KEY_VALUE_INFORMATION_CLASS" : [
        ("KeyValueBasicInformation", 0x0),
        ("KeyValueFullInformation", 0x1),
        ("KeyValuePartialInformation", 0x2),
        ("KeyValueFullInformationAlign64", 0x3),
        ("KeyValuePartialInformationAlign64", 0x4),
        ("KeyValueLayerInformation", 0x5),
        ("MaxKeyValueInfoClass", 0x6)
    ]
}
