#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" This module is able to extract type information from PDB files. """

import sys
import json
import pdbparse
from construct import Container

def generate_header(pdb, name):
    """ Write all type information gathered from pdb files into a json
        formated file.

    Keyword arguments:
    pdb  : path to the pdb file, which holds the information
    name : path to the new generated json file
    """
    with open(name, "w") as outfile:
        json.dump(get_type_information(pdb), outfile, indent=4, sort_keys=True)

def get_type_information(pdb):
    """ Opens a program database file and dumps all Structs, Unions and Enums
        into a json formated string.

    Keyword arguments:
    pdb    : path to program database file
    return : list of dictionarys, which
             holds offset, type, and variable name of each struct members,
             and each value and name of enum parameters
    """
    pdbfile  = pdbparse.parse(pdb)
    # generate all structs with type, argument names and offset of arguments and leave out forward references
    structs = [s for s in pdbfile.streams[2].types.values() if (s.leaf_type in ["LF_STRUCTURE", "LF_UNION"])
               and not s.prop.fwdref]
    structs_dict = {s.name[1:] : (hex(s.size), get_members(s)) for s in structs}
    # get all enums of pdb file, skip all forward references
    enums = [s for s in pdbfile.streams[2].types.values() if s.leaf_type == "LF_ENUM" and not s.prop.fwdref]
    enum_dict = {s.name : get_members(s, True) for s in enums}
    return [structs_dict, enum_dict]

def enum_to_hex(val):
    return hex(-1) if val == '\xff' else hex(val)

def get_members(obj, enum=False):
    """ Get all member variables for enums and structs as list.

    Keyword arguments:
    obj    : program database stream subfile ( for us structs/unions and enums
    enum   : boolean which indicates if given stream is an enum or struct/union (default False)
    return : list of tuples of member variable names, offset and type, or enum constant name and value
    """
    if enum:
        return [(sub.name, enum_to_hex(sub.enum_value))
                for sub in obj.fieldlist.substructs]
    else:
        return [(sub.name, hex(sub.offset), get_type(sub.index))
                for sub in obj.fieldlist.substructs]

def type_to_pointer(typename):
    """ Transforms a PDB type to a PDB pointer type. Because we just support 64 bit
    we know that we always have 64 bit pointers. """
    ptr_types = {
        "T_USHORT"   : "T_64PUSHORT",
        "T_SHORT"    : "T_64PSHORT",
        "T_UQUAD"    : "T_64PUQUAD",
        "T_RCHAR"    : "T_64PRCHAR",
        "T_UCHAR"    : "T_64PUCHAR",
        "T_WCHAR"    : "T_64PWCHAR",
        "T_ULONG"    : "T_64PULONG",
        "T_LONG"     : "T_64PLONG",
        "T_VOID"     : "T_64PVOID",
        "T_64PVOID"  : "T_64PVOID*",
        "T_PVOID"    : "T_64PVOID*",
        "T_64PVOID*" : "T_64PVOID**",
    }
    if typename in ptr_types.iterkeys():
        return ptr_types[typename]
    else:
        return typename + "*"

def get_type(typ):
    """ Returns the data type of each member variable from the pdb file structs.

    Keyword arguments:
        typ    : member description of pdb file struct
        return : type description of pdb file (PDB specific format)
    """
    if isinstance(typ, Container):
        return  {
            "LF_POINTER"   : lambda: type_to_pointer(get_type(typ.utype)),
            "LF_ARRAY"     : lambda: type_to_pointer(get_type(typ.element_type)),
            "LF_PROCEDURE" : lambda: "T_PVOID",
            "LF_MODIFIER"  : lambda: get_type(typ.modified_type),
            "LF_BITFIELD"  : lambda: get_type(typ.base_type),
        }.get(typ.leaf_type, lambda: typ.name if not typ.name.startswith("_") else typ.name[1:])()
    else:
        return typ if not typ.startswith("_") else typ[1:]

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Usage: <pdb file> <json file>")
    else:
        generate_header(sys.argv[1], sys.argv[2])
