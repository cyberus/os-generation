#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" This module fetches pdb files represented by GUID's from the microsoft
symbol server and saves them to the specified download folder. """

import os
import requests as req

def mkdir_p(path):
    """ Helper to create a directory path if it does not exist. """
    if not os.path.exists(path):
        os.makedirs(path)

def download(path, subsystem_guid_dict):
    """ Download the specified PDB's from the Microsoft symbol server to the specified directory.

    Keyword arguments:
    path                : destination path
    subsystem_guid_dict : windows subsystem name and guid dict
    """
    url = "http://msdl.microsoft.com/download/symbols/{0}/{1}/{0}"
    for name, guid in subsystem_guid_dict.items():
        mkdir_p(path)
        uri = url.format(name, str(guid))
        print("get:  {}".format(uri))
        resp = req.get(uri)
        # html response code should be 200, otherwise the pdb file is not present or could not be fetched
        if resp.status_code != 200:
            print("Failed to download {} make sure, that guid and name are correct".format(uri))
            sys.exit(1)
        with open(os.path.join(path, name), "wb") as pdbfile:
            pdbfile.write(resp.content)
