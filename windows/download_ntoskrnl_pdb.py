#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" Download ntoskrnl PDB files for every windows version specified in
artifacts/supported_windows_version.json (and which GUID's are available in artifacts/versions_ntoskrnl.csv)
and extract the type information from them. The output file is a json file containing dictionaries with
Windows struct and enum information containing names, typenames, offsets and sizes.

"""

import csv
import os
import shutil
import json
import argparse
import tempfile
from pdb.fetch_pdb import download
from type_generator.type_generator import get_type_information

kernel_pdb = "ntkrnlmp.pdb"

def parse_csv(file_path):
    """ Read and parse csv file.

    Keyword arguments:
    file_path : path to csv file
    returning list of version name, guid tuples
    """
    with open(file_path, "r") as csvfile:
        content = list(csv.reader(csvfile, delimiter=';'))
        return [tuple(line) for line in content[1:]]

def dump_json(filename, data):
    """ Dumps data to json.

    Keyword arguments:
    filename : json filename
    data     : data to be dump
    """
    with open(filename, "w") as json_file:
        json.dump(data, json_file, indent=4)

def parse_cmdline_args():
    """ Defines command line argument parser. """
    parser = argparse.ArgumentParser(description="Create Windows type information from GUID's.")
    parser.add_argument('--csv_filepath', action='store', help='CSV with Windows versions and GUIDs',
                        default='./artifacts/versions_ntoskrnl.csv')
    parser.add_argument('--out_file', action='store', help='Output JSON file',
                        default='./artifacts/win_types_ntoskrnl.json')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_cmdline_args()
    version_guid_list = parse_csv(args.csv_filepath)
    version_dict = dict()
    tmp_path = tempfile.mkdtemp()
    try:
        for version, guid, bits in version_guid_list:
            if bits == "64":
                download_path = os.path.join(tmp_path, guid)
                download(download_path, {kernel_pdb:guid})
                downloaded_file = os.path.join(download_path, kernel_pdb)
                version_dict[version] = get_type_information(downloaded_file)
        assert version_dict, "No type information retrieved. Are there any 64 bit GUID's in the csv_filepath?"
        dump_json(args.out_file, version_dict)
    finally:
        shutil.rmtree(tmp_path)
