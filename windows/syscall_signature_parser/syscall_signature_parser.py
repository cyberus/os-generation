#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" This module parses syscall parameters from known website sources.
A json file with the system call signatures is generated.
The json file has following format:
{
    "<system_call_name> : [
        [
            [
                "<[IN, OUT, INOUT]>",
                "<parameter_windows_type>"
            ],
            "<syscall_parameter_name>"
        ],

        ...
    ],

    ...
}

"""

import sys
import json
import re
import urllib2
import csv
import bs4
from quirks import quirk_calls

url_nt_in = "https://undocumented.ntinternals.net/{}"
url_wine  = "https://source.winehq.org/WineAPI/{}.html"

def split_params(params):
    """ Retrieve the list of comma seperated values surrounded by one pair of parantheses.

    Keyword arguments:
    params : string that contains parentheses
    return : a comma separated list that contains the content of the parentheses
    """
    expr = re.compile(r"\((.+)\)")
    vals = expr.search(params).group(1)
    # handle INOUT seperately
    def __prepare(parameter_line):
        """ Checks if the input is a parameter type and returns a tuple of parameter direction and type.
        """
        stripped_line = parameter_line.strip().split()
        if stripped_line[1] != "OUT":
            return stripped_line[0], stripped_line[1]
        else:
            return "INOUT", stripped_line[2]

    return [__prepare(x) for x in vals.split(",")]

def fetch_url(url):
    """ Request the url with a predefined generated request header.

    Keyword arguments:
    url    : request url of the destination server
    return : request content
    """

    hdr = {'User-Agent'      : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/'
                               '537.11 (KHTML, like Gecko) Chrome/23.0.1271.64'
                               ' Safari/537.11',
           'Accept'          : 'text/html,application/xhtml+xml,application/xml'
                               ';q=0.9,*/*;q=0.8',
           'Accept-Charset'  : 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding' : 'none',
           'Accept-Language' : 'en-US,en;q=0.8',
           'Connection'      : 'keep-alive'}
    req = urllib2.Request(url, headers=hdr)
    return urllib2.urlopen(req)

def get_params(url, identifier, replace, name_sep):
    """ Retrieve the system call parameters from the content of a given website url.

    Keyword arguments:
    url        : Destination server url
    identifier : class identifier of the html class descriptor
    replace    : list of the tags and control sequences which prevent
                 argument parsing
    name_sep   : html tag, that contains argument names
    return     : list of tuples which contain name and type of syscall
                 signature
    """
    site = None
    try:
        cont = fetch_url(url)
        site = bs4.BeautifulSoup(cont, "html.parser")
    except Exception:
        return None
    #search for function definition part of website
    param_list = next(n for n in site.find_all("pre") if "class" in n.attrs.iterkeys()
                      and n.attrs["class"][0] == identifier)
    v_names = [n.contents[0].strip() for n in param_list.find_all(name_sep)]
    if len(v_names) == 0:
        return None
    params = split_params(re.sub(replace, "", param_list.get_text()))
    return zip(params, v_names)

def fix_syscall_quirks(name, parameters):
    """ Check if the system call is a quirk and replace if applicable.

    Keyword arguments:
    name   : system call name
    params : system call parameters
    """
    for quirk in quirk_calls.get(name, []):
        parameters = replace_quirk(quirk, parameters)
    return parameters

def replace_quirk(quirk, parameters):
    """ Replaces quirked parameters.

    Keyword arguments:
    quirk      : tuple of system call parameters type and parameter name
    parameters : list of system call parameters
    """
    index, value = quirk
    parameters[index] = value
    return parameters

def get_syscall_param_by_name(name, undoc_syscall_urls):
    """ Parses https://undocumented.ntinternals.net/index.html website
    to generate syscall parameter information.

    Keyword arguments:
    name               : syscall name
    undoc_syscall_urls : system call names to urls dictionary
    return             : tuple of syscall argument type and name
    """
    try:
        url = url_nt_in.format(undoc_syscall_urls[name].encode("utf-8"))
    except KeyError:
        return get_syscall_param_by_name_alternative(name)
    #search for the function definition part of the website
    separator_list = "<br>|</br>|\r\n"
    return get_params(url, "FnDefinition", separator_list, "font")


def get_syscall_param_by_name_alternative(name):
    """ Parses https://www.winehq.org site to generate syscall parameter information.

    Keyword arguments:
    name : syscall name
    return tuple of syscall argument type and name
    """
    url  = url_wine.format(name)
    # searches html entry with parameter information
    return get_params(url, "proto", "\n", "tt")

def read_syscall_names(csv_filepath):
    """ Reads the syscall names from the csv file.

    Keyword arguments:
    csv_filepath : path to csv file
    """
    with open(csv_filepath, "rb") as csvfile:
        content = list(csv.reader(csvfile, delimiter=';'))
        return [name[0] for name in content[1:]]

def write_json(data, json_filepath):
    """ Creates a json file containing the generated syscall parameters.

    Keyword arguments:
    data          : data to write
    json_filepath : path to the json file to generate
    """
    with open(json_filepath, "w") as jsonfile:
        json.dump(data, jsonfile, indent=4)

def download_syscall_parameters(syscall_names_list, undoc_syscall_urls):
    """ Download and parse syscall definitions.

    Keyword arguments:
    syscall_names_list : name of system calls to download
    undoc_syscall_urls : system call names to urls dictionary
    return             : dictionary of syscall names and its parameters
    """

    syscalls = dict()
    found = 0
    not_found = 0
    for name in syscall_names_list:
        param = get_syscall_param_by_name(name, undoc_syscall_urls)
        if param:
            print("Found parameter:\t {}".format(name))
            found += 1
        else:
            print("Not found parameter:\t {}".format(name))
            not_found += 1
        syscalls[name] = param
    print("Found: {} Not Found: {} All: {}".format(found, not_found, len(syscall_names_list)))
    return syscalls

def fix_quirks(syscall_dict):
    """ Fix known quirks for syscalls.

    Keyword_arguments:
    syscall_dict : dictionary of downloaded syscall parameters
    """
    return {name : fix_syscall_quirks(name, args) for name, args in syscall_dict.iteritems()}

def init_undocumented_urls():
    """ The javascript file of 'undocumented.ntinternals.net' contains the url's to all system calls.

    return : Dictionary of system call name to 'undocumented' url
    """
    page = fetch_url(url_nt_in.format("files/treearr.js"))
    # we only need the variable description of each syscall and its link, so
    # delete not needed parts of js file
    link = ''.join(filter(lambda x: x not in '[]"', page.read()))
    und_data = {i.split(",")[0].strip(): i.split(",")[1].strip()
                for i in link.splitlines() if ".html" in i}
    return und_data

if __name__ == "__main__":
    undocumented_syscall_urls = init_undocumented_urls()
    if len(sys.argv) < 3:
        sys.exit("Usage: <csv file with syscall names> <output.json>")
    args = download_syscall_parameters(read_syscall_names(sys.argv[1]), undocumented_syscall_urls)
    write_json(fix_quirks(args), sys.argv[2])
