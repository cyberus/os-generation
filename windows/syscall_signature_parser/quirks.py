#!/usr/bin/env python2

# OS internals scraping toolkit
# Copyright (C) 2018 Cyberus Technology GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" List of quirks that are not or wrong documented in the known sources.
    This module contains all known quirks of
    https://undocumented.ntinternals.net/index.html
    The quirk parameter have the following structure:
    (parameter index, (type, name))

    the type is a tuple of:
    (parameter direction, parameter type)
"""
quirk_calls = {
    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FPort%2FNtConnectPort.html
    "NtConnectPort" : [(6, (("IN", "PVOID"), "ConnectionInfo"))],

    # https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FSection%2FNtMapViewOfSection.html
    "NtMapViewOfSection" : [(7, (("IN", "SECTION_INHERIT"), "InheritDisposition")),
                            (2, (("INOUT", "PVOID"), "BaseAddress"))],

    # remove '*' identifier from variable name ( make pointer)
    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtProtectVirtualMemory.html
    "NtProtectVirtualMemory" : [(1, (("INOUT", "PVOID"), "BaseAddress"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtFlushVirtualMemory.html
    "NtFlushVirtualMemory" : [(1, (("INOUT", "PVOID"), "BaseAddress"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Error/NtRaiseHardError.html
    "NtRaiseHardError" : [(3, (("IN", "PVOID"), "Parameters"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtAllocateVirtualMemory.html
    "NtAllocateVirtualMemory" : [(1, (("INOUT", "PVOID"), "BaseAddress"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtLockVirtualMemory.html
    "NtLockVirtualMemory" : [(1, (("IN", "PVOID"), "BaseAddress"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtUnlockVirtualMemory.html
    "NtUnlockVirtualMemory" : [(1, (("IN", "PVOID"), "BaseAddress"))],

    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/Memory Management/Virtual Memory/NtFreeVirtualMemory.html
    "NtFreeVirtualMemory" : [(1, (("IN", "PVOID"), "BaseAddress"))],

    # this call needs an array of handles, to avoid problems
    # https://undocumented.ntinternals.net/UserMode/Undocumented Functions/NT Objects/Key/NtCompactKeys.html
    "NtCompactKeys": [(1, (("IN", "PHANDLE"), "KeysArray"))],

    # This call is wrongly documented with a ULONG instead of a PULONG parameter
    #  https://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FNT%20Objects%2FKey%2FNtQueryMultipleValueKey.html
    "NtQueryMultipleValueKey": [(4, (("INOUT", "PULONG"), "BufferLength"))]
}
